# Rust Game Programming Book Outline

## Chapter 1 - Getting started

* Who is this book for?
* What we’ll be learning
  * A simple text-based game in plain Rust
  * A slightly more complex 2D game in the clicker style (using ggez)
  * A more complex simulation/agent-based game using Amethyst and ECS
* Needed Pre-existing Knowledge:
  * The Rust Programming Language
  * Programming Rust
  * Beginning Rust
* Gamedev Experience? Good to have, but not required.

## Chapter 2 - Rust

* Why Rust for game development?
  * Why C/C++ Has Been the Norm
  * Brief background on why most AAA engines have been C/C++
  * Garbage Collection
  * Memory Control
* Package system
  * Benefits of Cargo over make/cmake
  * Easier to share components
* Safety and Concurrency
  * Helps prevent bugs and regressions from reaching players
  * Allows for C-level speed, but the safety of higher level languages
* Cross-platform and WASM
  * Far easier to do cross-platform than in C++
  * Arguably no better language for WASM
* Easy to Use C/C++ Libraries
  * Rust-bindgen and FFI
* Rust GameDev Community
  * Rust GameDev Working Group
  * Mozilla (overlap between Servo and graphics, Azul. Using Chucklefish as a front-page case study)
  * Industry adoption (Embark, Seed, ReadyAtDawn, Chucklefish)
* Current Rust Game Development Ecosystem
  * gfx-rs
  * rendy
  * rodio
  * nalgebra
  * nphysics

## Chapter 3 - Making Games with Plain Rust

* Setting Up Your Development Environment (VScode or CLion)
* Coding Tic-Tac-Toe
* Simple AI
  * This one picks a square at random
  * CPU requriements between this and a Python implementation should be nearly the same
* Complex AI
  * Use minimax algorithm. This can generate a tree of all board states per move, and is thus “undefeatable”.
  * Demonstrate the difference in CPU time and RAM it takes the AI to calculate its results.

## Chapter 4 - Making Games with a Framework

* ggez
  * Description of what GGEZ is compared to a full engine
* Coding Cookie Clicker
  * Around the Complexity of Cookie Clicker
  * What a Clicker/Incremental game is
* iOS and Android Deployments
  * cargo-lipo
  * AndroidStudio

## Chapter N1 - Part 1: Making games with the ECS pattern

* Amethyst
  * Use a full game engine
  * Explain specs
  * Walk through the basic architecture
* Agent-based Simulation Game
  * Explain what Agent-based means
  * Explain what ECS systems are
  * Explain why they work well for this type of game
* Game Summary: Emergent Trading Game
  * Cities are simulated in the background
  * They produce various goods
  * Their populations grow and change
  * Economic Simulation: Player can buy and sell goods between cities
  * Stretch goals: Random World Events, complex resource properties, multiplayer...
* Separating Front-end and Back-end
  * Illustrate the benefits of a clean separation

## Chapter N2 - Part 2: Game Foundations

* Cities, Citizens & Wares
* Game Turns
* World Map
* Basic ASCII rendering
* Marketplace Trading
* Direct Trading
* UI states & scope
* Faking simulation with randomness

## Chapter N3 - Part 3: Agent-based simulation

* Emergent gameplay
* Basics of graph theory
* Letting citizens live their own (very simple) lives
* ECS and agents

## Chapter N4 - Part 4: Graphical interface

* Replacing our ASCII art with drawn art
* Advanced input support

## Chapter N5 - Part 5: Multiplayer

* Something simple such as tracking high scores
* Player registration system