# Diving in with Hangman

Our first game is a simple version of the game Hangman. We won't be using anything other than ASCII for the graphics, and all interaction will be via the terminal. The goal is ease you into game development with a low-level language without having to worry about art assets, sound, and all that.

## Work Environment

Since this is our first coding project, let's make sure you have your coding environment set up. As a relatively new language, Rust does not yet have the fancy IDEs one might expect for Java, C# or other popular languages. We do have two good choices that are often recommended in the Rust community: Visual Studio Code, and CLion. Visual Studio Code is free, while CLion is free for open-source projects. Both work on all three major operating systems. If you are used to other JetBrains products (IntelliJ, etc), you might prefer working with CLion. CLion also features a debugger, which can be useful.

We'll be using Visual Studio Code, due to the simplicity of getting setup.

### Setting up Visual Studio Code

1. Download Visual Studio Code from https://code.visualstudio.com
2. Open the `Extensions` section
3. Search for `Rust`
4. Install `Rust (rls) 0.5.3`

> RLS stands for Rust Language Server. It provides things like autocomplete, intellisense, and other capabilities for Visual Studio Code. Please note the latest version at the time of this writing was 0.5.3, but that may have changed.

### Terminal

The rest of what we need to do can be done in the terminal. VSC let's us open one via the `Terminal` menu at the top. Note that for the rest of this book, we'll see you are working a subdirectory of your home directory called `Projects`.

## Using a Version Control System (VCS)

> Any code we write, we keep in VCS if it is more than a few lines. We _strongly_ recommend you do the same.
> Most IDEs (including Visual Studio Code and CLion) have a VCS integration built-in. The `cargo new` command can even initialise an empty repository if you pass it a `--vcs` argument.
> A full tutorial on verison control is out of scope for this book, but there are many fine ones available. Future you will thank you.

## The Game

Hangman is a simple game that follows a specific series of steps that repeat until a win or loss condition is met. There is initial setup, and then what is often called the gameplay loop.

### Setting Up

1. The AI chooses a random word from a dictionary file
2. A series of lines is created below an ASCII-art gibbet

### Gameplay Loop

1. The player guesses a letter
2. If that letter is present in the word, it is placed in the appropriate blank spot, and we return to step 1
3. If that letter is _not_ present, then a piece of the hanged man (an arm, a leg, etc) appears on the gibbit, and we return to step 1

### Win or Lose

If the player manages to guess the word before his hanged man is complete, they win! If not, they lose!

## Creating the Crate with Cargo

The first step in creating a new Rust project is almost always `cargo new`. Let's start with that:

1. Open up your Terminal or PowerShell window
2. Go to your `Projects` directory
3. Enter the command `cargo new hangman --bin`

On Linux or macOS, you should see output similar to below:

```bash
$ cargo new hangman --bin
     Created binary (application) `hangman` package
```

If you look in that directory, you will see that we now have a new directory called `hangman`. Run `cd hangman` to get into that directory. It should look like this:

```bash
$ ls
Cargo.toml src
```

Because we used the `--bin` flag, we also have `src/main.rs` with a main function that prints `Hello World!`. You can give it a try, like so:

```bash
$ cargo run
   Compiling hangman v0.1.0 (/Users/fletcher/Projects/hangman)
    Finished dev [unoptimized + debuginfo] target(s) in 1.94s
     Running `target/debug/hangman`
Hello, world!
```

### Loading the Word List

We're going to use a simple list of ten words, in a file called `words.txt`. This file will be at `hangman/words.txt`. You can pick any words you like. We used the ones below:

```text
channel
wilderness
shrill
high
nation
uneven
dazzling
rejoice
trade
female
chance
abashed
```

In Rust, we can use the `include_str!` macro to read the file:

```rust
const WORDS: &str = include_str!("words.txt");
```

This should go near the very top of your `main.rs` file. When the game gets compiled, Rust will look for that file and read in its contents. Note that it _does not_ tokenize them. It is read in as one long string. It looks like this: `channel\nwilderness\nshrill...` and so on. That's ok for now; we'll tokenize them soon.

> Using `include_str!` adds the words *into the executable* at compile time. You don't need to ship the file with it.

### Random Number Generators

Since we want to choose a word at random, we need something to generate that random number. We'll use the `rand` package.

In your Cargo.toml file, add the following dependency:

```toml
rand = "0.6.4"
```

> Please note the version may have changed by the time you are reading this.

In `main.rs`, at the top of your file, add the line:

```rust
use rand::{self, ThreadRng, seq::SliceRandom};
```

This will give us a `ThreadRng` we can use. Since our game has just one thread, that's all we need.

### Starting on Main

Finally! Let's clear out the main function so you have this:

```rust
fn main() {

}
```

And we'll get started with the game code. Ready? Here we go!

## Game Code

We have two tasks that need to be done before we can start the primary game loop:

1. Split up the word list into an array
2. Create our random number generator

### Word List

For your first line in `main`, put this:

```rust
let words = WORDS.split_whitespace().collect::<Vec<_>>();
```

This little line of code is doing a lot for us:

1. Takes the single string and splits it on whitespace
2. Collects the results into a Vector (array)
3. Binds the results to the variable words

You can see a common pattern in Rust here, which is using `collect()`. While not a pure functional language, Rust leans strongly in that direction. Often, the compiler can better optimize code if it is written using techniques like these, rather than constructs like looping.

### RNG

This next line is even easier:

```rust
let mut rng = rand::thread_rng();
```

Note the difference in mutability here. The RNG we expect to change; the word list should not. Being consienctious about variable mutability leads to better, safer code. You know what can and can't change, and the compiler can perform better operations if it has that information available.

### Game Loop

All games are giant, complex, infinite loops at their heart and ours is no different. Let's start with an empty loop:

```rust
loop {

}
```

There is, however, a lot of logic packed into these loops; let's start adding some of our own.

### Adding the Menu

The first interactive thing you see after starting the game is usually the main menu. Ours is going to be really simple, but we can extend it later.

Let's print the menu first:

```rust
fn main() {
    let words = WORDS.split_whitespace().collect::<Vec<_>>();
    let mut rng = rand::thread_rng();

    loop {
        println!("\nHangman:");
        println!("1. [N]ew game");
        println!("2. [Q]uit");

        let answer = read_input("> ").unwrap_or_default().to_ascii_lowercase();
}
```

This will print the menu, show a little prompt (`>`) and wait for the player to type something and press Enter. If you run this, you will get an unused variable warning and it will keep looping until you kill the program.

> We'll define `read_input` in the next section

Let's handle the player choices. Put the following right `let answer`:

```rust
match answer.as_str() {
    "1" | "n" => game::play(&words, &mut rng),
    "2" | "q" => break,
    _ => println!("Unknown option: {:?}", answer),
}
```

The `match` expression (similar to `switch` in other languages, but more powerful) lets us choose different options based on the value. You could achieve the same effect with a set of `if` / `else` statements, but this is cleaner.

Some nice features include the ability to assign a given action to multiple values if you separate them with `|`. So in the code above, entering *either* `1` or `n` will call the `game::play_game` function.

The second case will simply break out of the loop, quitting the game. We could also just say `return` here instead.

And the final option (the underscore) is a catch-all arm: it will handle all the other cases (similar to `default` in C or C++). Contrary to similar constructs in most other languages, `match` **has to** handle all possible alternatives.

Try removing the last line and see what happens.

### Reading Player's Input

This is a text game so we need to read what the player enters on the keyboard. We will define a function that will show a prompt on the screen and read anything typed in followed by Enter.

```rust
fn read_input(prompt: &str) -> Result<String, std::io::Error> {
    print!("{}", prompt);
    std::io::stdout().flush()?;

    let mut input = String::new();
    std::io::stdin()
        .read_line(&mut input)
        .map(|_| input.trim().to_string())
}
```

The return value is a `Result` so we can differentiate between the successful case (in which we return the input) and an error (which we pass back to the caller so they can handle it).

`print!` is similar to `println!` but it doesn't add a line break, so any other text will be shown on the same line as the prompt. Since Rust buffers the input and output by default, we will need to call `flush` in order for it to show immediately. Try removing it and see what happens!

Next, we create the `String` we want to record the input in and pass it to `read_line` and finally strip all leading and trailing whitespace. The `read_line` function appends the newline character (`\n`) at the end of the `String` which is why we do the `trim` at the end.

### New Module: `game.rs`

The main menu is calling a `game::play` function which doesn't exist yet. We can keep all code in `main.rs`, but it's often better to have things in their own modules.

Create a file called `game.rs` in the `src` directory and put this function there:

```rust
pub fn play_game(rng: &mut ThreadRng, words: &[&str]) {
    unimplemented!()
}
```

> It is possible to add modules into subdirectories, but our project is so small it's not really necessary.
> `unimplemented!()` is a handy macro that will panic if it's called. We'll implement it later.

To play the game we'll need both the random generator and the word list. We also need to bring `ThreadRng` into scope in our `game` module too (just like we did in `main`). Add this at the beginning of `game.rs`:

```rust
use rand::ThreadRng;
```

Just adding the file is not enough, though. We need to tell Rust about it too.

In `main.rs`, add the `mod` declaration after any `use` blocks:

```rust
mod game;
```

If you compile the project now, it will include the module. You will see Rust warning about unused arguments in the `play` function.

## Coding the Game

Time for the best part: the game logic! In our `play()` function in `game.rs`, let's do some initial setup:

```rust
    let mystery_word = words.choose(rng).unwrap_or(&"default");
    let max_misses = assets::STAGES.len() - 1;

    let mut hits = vec![];
    let mut misses = vec![];
```

This picks a word, figures out how many incorrect letters are allowed, and sets up two empty data structures to track them. These are all things we want to only happen once per game. Next we can move on playing the game.

In the next sections, we will first show the current progress (even if there's none yet) and then ask the player's guess. Finally, we evaluate the guess and either quit or keep going.

### Drawing the Hanged Man

```rust
    let index = misses.len().min(assets::STAGES.len() - 1);
    println!("{}", assets::STAGES.get(index).unwrap_or(&""));
```

These two lines are responsible for drawing the hanged man at his current stage. The index is used to determine which ASCII art picture to draw.

### Adding the graphics

We need to create the `assets` module and add our pictures there so we can print them out. Create a new file called `assets.rs` and add an array of 11 string slices in there:

```rust
pub const STAGES: [&str; 11] = [r"", r"", r"", r"", r"", r"", r"", r"", r"", r"", r""];
```

This is a fixed-length array of `11` elements that can't change (see `const`). Each element is a raw empty string right now.

You can fill in the various stages of the guessing graphics now. For example, this is the final "you lost the game" picture:

```rust
    r"
    -------------\
   |             |
   |             |
  / \            |
  \ /            |
  /|\            |
 / | \           |
   |             |
  / \            |
 /   \           |
                /|
               / |
              /  |
-----------------------
",
```

> You can see the full contents in the source code accompanying this book.

We need to include the module in our `main.rs` in order to use it:

```rust
mod assets;
```

### Showing Progress

The next section of code fills in the blanks with all the guessed letters:

```rust
    let progress = mystery_word
        .chars()
        .map(|letter| hits.iter().find(|&&guess| guess == letter).unwrap_or(&'_'))
        .map(ToString::to_string)
        .collect::<Vec<_>>()
        .join(", ");
    println!("{}", progress);
```

Don't panic!()! This is a good example of chaining functions in Rust, which is quite common to do. Let's step through this:

1. chars() gives us an iterator over ever letter in the mystery word
2. map applies the function find to every successful guess ("hit") made so far. It returns either the guessed letter or a '_'.
3. We then turn it into `String`, and join them all together, separated by commas, and print it out. This gives us the bar of letters and underscores.

```rust
    if !misses.is_empty() {
        println!(
            "Your misses so far: {}",
            misses
                .iter()
                .map(ToString::to_string)
                .collect::<Vec<_>>()
                .join(", ")
        );
    }
```

All this section does is tell the player what they've already guessed that was incorrect, so they don't re-guess the same incorrect letters.

### Victory Check

Every iteration, we need to see if the player has won. We can do that with this line:

```rust
    let victory = mystery_word.chars().all(|letter| hits.contains(&letter));
```

This checks if all the characters in the mystery are present in the hits vector. If so, victory is true. If not, false.

```rust
    if victory {
        println!("You win. Congratulations!");
        match misses.len() {
            0 => println!("You made no mistakes! Are you a psychic?"),
            misses if misses == max_misses - 1 => {
                println!("You got it on your last attempt. That was a close shave!")
            }
            count => println!("You made {} incorrect guesses.", count),
        }

        break;
    }
```

If `victory` is true, we want to print out a victory message and, break out of the gameplay loop, and return to the main menu.

If `victory` is false, we need to check if the player lost, which we can do with:

```rust
    if misses.len() >= max_misses {
        println!("You took too many guesses. You lose.");
        println!("The word was: {:?}", mystery_word);
        break;
    }

    println!();
```

We print a message indicating the loss, and break out of the loop.

### Taking a Guess

```rust
    let input = match crate::read_input(r#"Guess a letter or write "quit": "#) {
        Ok(input) => input,
        Err(_) => continue,
    };
```

> This is the same `read_input` as in our crate root. It prompts the player for some text.

First, we read their input. If it is valid we'll move on. If not, we just continue to the next loop iteration; we can tell which is which because the `read_input` function returns a `Result`.

```rust
    let action = handle_input(&input);
```

Next, we use another helper function, `handle_input`, to figure out what action the user intended.

```rust
    match action {
        Guess(letter) => {
            if mystery_word.contains(letter) {
                hits.push(letter);
            } else {
                misses.push(letter);
            }
        }
        Invalid(input) => println!(
            r#"Unexpected input: {:?}. Enter either a letter or the word "quit"."#,
            input
        ),
        Quit => {
            println!("Quitting the game.");
            break;
        }
    }
```

1. And the last step is matching on the `action`. If it is a valid Guess, we check if the letter is in the mystery word. If so, we add it to the hits vector. If not, the misses.
2. If it was Invalid, we print a message, and go to the iteration.
3. If the player quit, we print a message and break from the loop.

This continues until win or less conditions are met.

And that's the entire game loop! This is a very simple game, but most games share the same core idea of loops getting player input, changing the game state, displaying it, and repeating.

## Extra Credits

This is quite a respectable game of Hangman we've built here. Hooray us! It's always great to finish a game, however simple.

Before we call it done and upload it to all the app stores though, let's think about ways we could improve it. We will not go into the same amount of detail, but we will suggest a few things you can try on your own with enough info to get you started.

### Custom Graphics

Right now, the various stages of the hangman ASCII pictures are hardcoded into the game. That is fine and it does mean we never have to worry about not being able to load the picture.

But there's a ton of reasons to allow players to bring their own pictures.

Depending on who plays it, the paper variant of Hangman can have different number of attempts (i.e. drawn lines). Some games start with the gallows fully constructed and just finish the rope and the figure. Others start with the pole already erect, but nothing else. Yet others start completely blank or (like we do) with just the line representing the ground.

Plus, think of all the seasonal content you can sell as a DLC! Nothing says Xmas like a hangman wearing a festive hat!

To do this, you would want to have an option to read a custom set of images into a `Vec<String>` and use that instead of the default graphic. Maybe check a `mods` directory.

You would also want to show the various options either in the menu or possibly via a command line switch.

### Custom Dictionaries

Make a themed word list (after clearing it with your legal department of course)! Or the seven dwarves (perfect for kids!), or an educational version listing all the world's nations. How about all the fancy words from the Sherlock Holmes novels that are in the public domain? Indubitably!

Having a command line switch makes it convenient to ship themed pack. You just include the right dictionary and set the right options in your game's launcher -- no need to recompile the game.

But having an option to switch the word lists in the main menu would make it easy for all the aspiring game modders out there.

### Winning Streak

Instead of just recording the score of a single guess, you can let the player start a new game and add *that* score as well.

This can add an interesting dimension if the whole streak is lost when the player loses. Do you want to record what you've got or risk it all for a higher score?

## Shipping Your Game

When you're ready to ship your game, there's a lot to coordinate: compilation for multiple platforms, distribution, advertising, support, and more. Here we are going to focus on getting your application ready to run on the three major platforms: Windows, macOS, and Linux. We'll do it via the CLI first, and then talk about ways to automate this.

### Release Builds

This is a common source of confusion for Rust coders, even experienced ones. Against all odds, we're ready to release our beautiful application upon the world...and it runs horribly slowly. The most common cause is forgetting to add `--release` to your build command.

During the development cycle, `cargo build` is common. This makes a build of your application that includes the debugging symbols, plus it is a lot faster. The compiler doesn't make as many optimization passes. Changing that to `cargo build --release` will remove the symbols, and do a lot more optimization. The speed-up, in most cases, is very noticeable.

Another optimization to turn on is _Link-Time Optimization_ (LTO). This allows the compiler to spend more time optimizing during link time. It will add to the total compile time, but the final result can be faster.

These are two of the easiest, and most impactful, things to make sure are done before release. The flags work the same on all three platforms. Now let's look at the platform specific things.

### Windows

Windows is, by far, the most popular platform for gaming; we'll want to make sure to provide an excellent experience for them, which we hope will translate to fewer support tickets!

#### Raw .exe

For very simple applications that do not need supporting files (graphics, audio, etc), it can be easiest to distribute the executable produced by the `cargo build --release` project. Games often have a significant amount of assets that live in external files, such as 3D models, audio, and other resources that are not baked into the bare executable. To be able to distribute those, Windows has something called an MSI.

### Cross-compiling

There is a technique called cross-compiling that let's you build a binary for one operating system on another; building Windows version on a Linux system, for example. This is possible with Rust, but more difficult, and something we'll explore later.

For now, we're going to use a service to build versions for all three platforms.

### Travis CI

Unless you have a Mac, Linux and Windows computer to build the individual binaries, we'll use a platform called Travis CI to start us off. It is free for open-source projects, and you can find it [here](https://travis-ci.org). What this will let us do is include a file in our repository that Travis will read. In the background, it will create virtual machines, run the commands you tell it, and give you back the results. In the next chapter, we'll show an example Travis CI file.

## Conclusion

Congratulations! We've written our first game in Rust!

While this first game is not very complex, it isn't trivial either. Not once in our development did we have to worry about allocating and freeing memory, tracing down memory leaks, or learning arcane valgrind invocations to troubleshoot. It is something of a mantra in both the Rust and Haskell communities that "if it compiles, it runs". This isn't _always_ true, of course, but the Rust compiler can get us pretty close.
