use std::{
    fs::File,
    io::{prelude::*, Error},
    path::Path,
};

const SCORE_FILE_PATH: &str = "high-score.txt";
const EMPTY_NAME: &str = "empty";

fn ensure_score_file_exists() {
    let path = Path::new(SCORE_FILE_PATH);
    if !path.exists() {
        // Just create an empty file
        let _ = File::create(path);
    }
}

fn parse_line(line: &str) -> (String, usize) {
    let mut components = line.split('\t');

    let name = components.next().unwrap_or(EMPTY_NAME).to_string();
    let score = components
        .next()
        .map(|s| s.parse().unwrap_or(0))
        .unwrap_or(0);
    (name, score)
}

pub(crate) fn load() -> Result<Vec<(String, usize)>, Error> {
    let default_score = (EMPTY_NAME.to_string(), 0);
    ensure_score_file_exists();
    let file_contents = std::fs::read_to_string(SCORE_FILE_PATH)?;
    let scores = file_contents
        .trim()
        .lines()
        .map(parse_line)
        .chain(std::iter::repeat(default_score))
        .take(10)
        .collect::<Vec<_>>();
    Ok(scores)
}

pub(crate) fn save(scores: &[(String, usize)]) -> Result<(), Error> {
    let mut file = File::create(SCORE_FILE_PATH)?;
    for (name, score) in scores {
        writeln!(file, "{}\t{}", name, score)?;
    }
    Ok(())
}

pub(crate) fn add_high_score(name: &str, score: usize) {
    if let Ok(mut scores) = load() {
        scores.push((name.to_string(), score));
        scores.sort_by_key(|(_, score)| *score);
        scores.reverse();
        let _ = save(&scores);
    }
}

pub(crate) fn is_new_high_score(score: usize) -> bool {
    if let Ok(scores) = load() {
        let lowest_score = scores.iter().map(|(_, score)| *score).min().unwrap_or(0);
        return lowest_score < score;
    }
    false
}

pub(crate) fn print_table(scores: &[(String, usize)]) {
    let longest_name = scores.iter().map(|(name, _)| name.len()).max().unwrap_or(0);
    for (index, (name, score)) in scores.iter().enumerate() {
        println!(
            "{}.\t{:width$}\t{}",
            index + 1,
            name,
            score,
            width = longest_name
        );
    }
}
