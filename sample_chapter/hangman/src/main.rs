use std::io::Write;

mod assets;
mod high_score;
mod game;

const WORDS: &str = include_str!("words.txt");

fn read_input(prompt: &str) -> Result<String, std::io::Error> {
    print!("{}", prompt);
    std::io::stdout().flush()?;

    let mut input = String::new();
    std::io::stdin()
        .read_line(&mut input)
        .map(|_| input.trim().to_string())
}

fn main() {
    let words = WORDS.split_whitespace().collect::<Vec<_>>();
    let mut rng = rand::thread_rng();

    loop {
        println!("\nHangman main menu:");
        println!("1. [N]ew game");
        println!("2. [H]igh score");
        println!("3. [Q]uit");

        let answer = read_input("> ").unwrap_or_default().to_ascii_lowercase();
        match answer.as_str() {
            "1" | "n" => game::play_game(&mut rng, &words),
            "2" | "h" => match high_score::load() {
                Ok(high_scores) => {
                    println!("High score:");
                    high_score::print_table(&high_scores);
                }
                Err(error) => println!("Error loading high score: {}", error),
            },
            "3" | "q" => break,
            _ => {}
        }
    }
}
