use rand::{self, rngs::ThreadRng, seq::SliceRandom};

use crate::read_input;
use crate::assets;
use crate::high_score;

enum Action<'a> {
    Guess(char),
    Invalid(&'a str),
    Quit,
}

fn handle_input(input: &str) -> Action {
    let trimmed = input.trim();
    if trimmed.to_ascii_lowercase() == "quit" {
        return Action::Quit;
    }

    if let Some(guess) = trimmed.chars().next() {
        if guess.is_alphabetic() && trimmed.len() == 1 {
            return Action::Guess(guess.to_ascii_lowercase());
        }
    }

    Action::Invalid(input)
}

pub fn play_game(rng: &mut ThreadRng, words: &[&str]) {
    let mystery_word = words.choose(rng).unwrap_or(&"default");
    let max_misses = assets::STAGES.len() - 1;

    let mut hits = vec![];
    let mut misses = vec![];

    loop {
        use self::Action::*;

        let index = misses.len().min(assets::STAGES.len() - 1);
        println!("{}", assets::STAGES.get(index).unwrap_or(&""));

        let progress = mystery_word
            .chars()
            .map(|letter| hits.iter().find(|&&guess| guess == letter).unwrap_or(&'_'))
            .map(ToString::to_string)
            .collect::<Vec<_>>()
            .join(", ");
        println!("{}", progress);

        if !misses.is_empty() {
            println!(
                "Your misses so far: {}",
                misses
                    .iter()
                    .map(ToString::to_string)
                    .collect::<Vec<_>>()
                    .join(", ")
            );
        }

        let victory = mystery_word.chars().all(|letter| hits.contains(&letter));
        if victory {
            println!("You win. Congratulations!");
            match misses.len() {
                0 => println!("You made no mistakes! Are you a psychic?"),
                misses if misses == max_misses - 1 => {
                    println!("You got it on your last attempt. That was a close shave!")
                }
                count => println!("You made {} incorrect guesses.", count),
            }

            let score = max_misses - misses.len();
            if high_score::is_new_high_score(score) {
                if let Ok(name) = read_input("You've got a high score! Enter your name: ") {
                    high_score::add_high_score(&name, score);
                }
            }

            break;
        }
        

        if misses.len() >= max_misses {
            println!("You took too many guesses. You lose.");
            println!("The word was: {:?}", mystery_word);
            break;
        }

        println!();
        let input = match read_input(r#"Guess a letter or write "quit": "#) {
            Ok(input) => input,
            Err(_) => continue,
        };

        let action = handle_input(&input);

        match action {
            Guess(letter) => {
                if mystery_word.contains(letter) {
                    hits.push(letter);
                } else {
                    misses.push(letter);
                }
            }
            Invalid(input) => println!(
                r#"Unexpected input: {:?}. Enter either a letter or the word "quit"."#,
                input
            ),
            Quit => {
                println!("Quitting the game.");
                break;
            }
        }
    }
}
