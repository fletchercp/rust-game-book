# Writing a text-based Hangman game

## Hangman

We will write a single-player computer version of the Hangman game:

![Photo of a game of Hangman in progress on paper](360px-Hangman_game.jpg)

(source: https://en.wikipedia.org/wiki/File:Hangman_game.jpg, created
by: McGeddon, license: CC0)

The computer will think of a random word and the player's job is to
guess it letter by letter.

Every time the player guesses a letter that the word contains, it gets
filled in the blanks.

When the player makes a wrong guess, that letter gets crossed out and
the computer draws a bit of the gallows picture.

The game ends when the player guesses the entire word or when they
made enough incorrect guesses that the full picture is drawn.

### What we'll do

The game will be text-based, using ASCII graphics for the hangman
picture. It will be played on the command line.

Despite being simple, we will treat it like a real game. It will keep
track of the high score locally, have robust error handling, work on
Windows, OSX and Linux and we will show how to build it for all these
platforms.


## Steps

1. Hello world
   * always build with `--release` for testing speed
2. Read input, print it out
3. Validate: only alphabetic chars, only 1 char at a time
3. Loop: read char, print it out, add "quit" command (or mention Ctrl+C / Ctrl+Z on Windows)
4. Add a sample word, remember all guesses, print out guesses and blanks
5. Add multiple words, include the `rand` crate
6. Add limit to the number of guesses, fail the game when reached
6. Loop to start a new game / quit
7. Read word-list from a file
    * Use the `include!` macro to ship the word list with the exe
    * Note how this will never fail (no file not exist / can't be read
      errors)
    * Self-contained executable
    * Provide a sample word list for download on the pragprog website
7. Draw the hang-man stick figure to show the number of attempts
8. Keep track of high scores
    * On victory, the game will ask for player's nick
    * High scores will be stored in a text file next to the game
      executable


## Extra credit

These are optional steps the reader can do. We will provide a rough
guideline, point to useful crates, maybe some code snippets.


### Custom word list support

Let the players bring their own word list:

    ./hangman --dictionary game-of-thrones-characters.txt

This can be an introduction to handling command-line arguments.


### Tests

Show `cargo test`, how to verify some of the functions we use.


### AI

Write a simple AI that can play the game. Mention language letter
frequency.


## Shipping ##

* always run `cargo build --release` for benchmarks and shippnig
* enable LTO
* platform-specific considerations
    * CR LF (Windows) vs. LF (OSX, Linux) for text files
  * Windows executable icon?
    * do these exist for CLI tools as well or just for the windowed ones?
* mention musl for linux
* build binaries on travis ci + appveyor (or gitlab ci?)


## Summary and comparison with other languages

NOTE: this could eventually go to the "Why Rust" section of the book
or something.

### Rust Pros

* no need for null checks everywhere
* use include! macro to ship the words list with the executable
* single binary
* compiler is your mate
* fast
* Your players don't have to install Python (or Ruby, Node.js, JVM, .NET)
   * or use something that ships the whole thing with your game
   * size increase, complexity, possible multiplatform issues
* Easy C interop when you need it, more natural than with Python or
  Java
* In general Rust is still pretty close to the gamedev standard (C++),
  just safer and shedding some of the historical cruft


### Rust Cons

* ownership does make things a little more complex
* the syntax is a bit noisier (but no by much (hopefully :D))
* slower build / compile times (but getting better, plus `cargo check`)
* debugging, engines & libraries: less mature, fewer choices (but
  still feasible!)


### Other languages

* C/C++: easy segfaults -- especially tough on beginners
* C/C++: kind of hard to discover / add libraries (cargo FTW)
* Python, JVM, Ruby, JavaScript: you're asking users to install the
  language runtime and/or have a more complex build process
* JavaScript: native for the web, but you can use Rust with wasm
* Python, Ruby, JS: slower. Not a big deal with Hangman but can be huge
  in games (even visually simple ones like roguelikes because of worldgen
  or AI)
* GC-collected langs: can be slow but more importantly unpredictable,
  less control over the non-memory resources (textures etc.)
